import * as React from 'react';
import { useAnimalSound } from './hooks';

interface AnimalSays {
    animal: string,
    times?: number
}

const Says: React.FC<AnimalSays> = ({ animal, times=5}:AnimalSays) => {
    const sound = useAnimalSound(animal)

    const says = [];
    for (let i =0; i<times; i++) {
        says.push(<p key="{`name-${i}`}">{sound}</p>);
    }

    return (
        <div>{ says }</div>
    )    
}

export default Says