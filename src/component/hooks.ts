export const useAnimalSound = (animal: string) => {
    switch(animal) {
        case 'cow':
            return 'Mooo!'
        case 'dog': 
            return 'Woof'
        default:
            return 'Wow??'
    }
}